import logger from "./logger.mjs";

class Counter {
    constructor(initialValue = 0) {
        this.value = initialValue;
    }

    increase() {
        this.value++;
        logger.info(`[COUNTER] increase ${this.value}`);
    }

    zero() {
        this.value = 0;
        logger.info(`[COUNTER] zeroed ${this.value}`);
    }

    read() {
        return this.value;
    }
}

export default Counter;