import logger from './logger.mjs';

/** @type {import('express').RequestHandler;} */
const endpoint_mw = (req, res, next) => {
    logger.info(`[ENDPOINT] ${req.method} '${req.url}'`);
    next();
};

export default endpoint_mw;